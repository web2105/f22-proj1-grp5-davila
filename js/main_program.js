/* 
Kayci Nicole Davila student id:2141560
Alex Alonso Davila student id:2132425
*/
'use strict';

//taking every form element one by one
let pid = document.querySelector("#projectId");
let owner_name = document.querySelector("#ownerName");
let title = document.querySelector("#title");
let category = document.querySelector("#category");
let status_elem = document.querySelector("#status");
let hours = document.querySelector("#hours");
let rate = document.querySelector("#rate");
let descr = document.querySelector("#description");

/*regex patterns in there*/
//Alex Davila
let proj_pattern = /^[a-zA-Z]{1}[\w$_\-]{2,9}$/;
let owner_pattern = /^[a-zA-Z]{1}[\w\-]{2,9}$/;
let proj_title = /^[a-zA-Z]{3,25}/;
let category_patt=/^[a-zA-Z]{3,25}/;
let status_patt=/^[a-zA-Z]{3,25}/;
let nber_hours = /^[0-9]{1,3}$/;
let rate_pattern = /^[0-9]{1,3}$/;
let proj_descr =  /^[\s\S]{3,65}$/;


let proj_obj = createProjectObject();

//global variables
let arr = [];
let tble = document.querySelector("#tb1");
let btn = document.querySelector("#add");
let rst = document.querySelector("#rst");
let del = document.querySelector(".del_btn");
let edt = document.querySelectorAll("#ed");
let saveToLocal = document.getElementById("save");
let clearStorage = document.getElementById("clear");
let appendStorage = document.getElementById("append");
/*making all 8 fields required and bolded*/
//Alex Davila

//Alex an Kayci
document.addEventListener('DOMContentLoaded',function(){
    
    enable_disable_button(btn,false);
    
   
    //Event listeners for the form
    set_elem_required("projectId");
    pid.addEventListener('blur',function() {validateElement(pid,proj_pattern,"Wrong format for ProjectId...");validateAll();});
    set_elem_required("ownerName");
    owner_name.addEventListener('blur',function() {validateElement(owner_name,owner_pattern,"Wrong format for Owner...");validateAll();});
    set_elem_required("title");
    title.addEventListener('blur',function() {validateElement(title,proj_title,"Wrong format for Title...");validateAll();});
    set_elem_required("category");
    category.addEventListener('blur',function(){validateElement(category,category_patt,"Wrong format for Category...");validateAll();});
    set_elem_required("hours");
    hours.addEventListener('blur',function() {validateElement(hours,nber_hours,"Wrong format for Hours...");validateAll();});
    set_elem_required("rate");
    rate.addEventListener('blur',function() {validateElement(rate,rate_pattern,"Wrong format for rate");validateAll();});
    set_elem_required("status");
    status_elem.addEventListener('blur', function (){validateElement(status_elem,status_patt,"Wrong format for Status...");validateAll();});
    set_elem_required("description");
    descr.addEventListener('blur', function (){validateElement(descr,proj_descr,"Wrong format for Description...");validateAll();});


    //event listenersfor the add and reset button
    btn.addEventListener('click',function(event){updateProjectsTable(event,arr);});
    rst.addEventListener('click',function(event){resetProject(event);});

    //event listeners for read the storage and clear from it
    let getFromLocal = document.getElementById("load");
     getFromLocal.addEventListener('click',function(){readFromLocalStorage();});

    clearStorage.addEventListener('click', function(){clearLocalStorage();});

   // saveToLocal.addEventListener('click',function(){saveToLocalStorage();});

    /*Fetching images that on click will sort the column*/
    let sortId = document.getElementById("sortProjIds");
    let sortOwner = document.getElementById("sortOwners");
    let sortTitle = document.getElementById("sortTitles");
    let sortCategory = document.getElementById("sortCategories");
    let sortHours = document.getElementById("sortHours");
    let sortRate = document.getElementById("sortRates");
    let sortStatus = document.getElementById("sortStatus");
    let sortDescription = document.getElementById("sortDescriptions");

    /*Event listeners for sorting images*/
    sortId.addEventListener('click', function(){sortcolumn(arr,compareIds);});
    sortOwner.addEventListener('click', function(){sortcolumn(arr,compareOwners);});
    sortTitle.addEventListener('click', function(){sortcolumn(arr,compareTitles);});
    sortCategory.addEventListener('click', function(){sortcolumn(arr,compareCategories);});
    sortHours.addEventListener('click', function(){compareHours(arr);});
    sortRate.addEventListener('click', function(){compareRates(arr);});
    sortStatus.addEventListener('click', function(){sortcolumn(arr,compareStatus);});
    sortDescription.addEventListener('click', function(){sortcolumn(arr,compareDescriptions);});
    
});

  
    
    


