/* 
Kayci Nicole Davila student id:2141560
Alex Alonso Davila student id:2132425
*/
'use strict';
/**
 * 
 * @description This is a toggle function that enables and disables the add button
 */
function enable_disable_button (btn,valid){
  
    if(valid){
        btn.removeAttribute('disabled');
        btn.style.border = "1px outset lightblue";
        btn.style.backgroundColor = "#0977b3"
        btn.style.color = "white";
        btn.style.padding = "10px 50p";
        btn.style.margin = "5px 60px";
        
    } else {
        btn.setAttribute('disabled','disabled');
        btn.style.border = "1px outset lightgrey";
        btn.style.backgroundColor = "lightgrey"
        btn.style.color = "white";
        btn.style.padding = "10px 50p";
        btn.style.margin = "5px 60px";
    }
}
    /**
     * @description This function sets the background color of every row
     */
    function setRowColors (){
        for(i=1;i<rows.length;i++){

            if((i%2) !== 0){
        
                rows[i].style.backgroundColor = "#bdc1c7";
            } else { rows[i].style.backgroundColor = "white";}
                    
        }
    }


/**
 * @description This function takes an elementid and sets it as required
 * @param {HTMLElement} elemId 
 */
function set_elem_required(elemId){
    let myInput = document.getElementById(elemId);
    myInput.setAttribute('required','required');

    /*Finding the right label for the element inputed*/
    let labels = document.getElementsByTagName('label');
    for(let i=0; i < labels.length; i++){

        if (labels[i].htmlFor == elemId){

            /*making it bold*/
            labels[i].style.fontWeight = 'bold';
        }
    }

}
/**
 * @description This function takes an elemid, feedbackText, and isValid to displays 
 * proper validation info based on boolean
 * @param {HTMLElement} elemid 
 * @param {String} feedbackText 
 * @param {boolean} isValid 
 */
function setElementFeedback(elemid,feedbackText ,isValid) {

    let elem = document.getElementById(elemid);
    let green_img = document.createElement('img');
    let red_img = document.createElement('img');
    let divId = elem.id + "_div";
    let container = document.getElementById(divId);
   
    /*This loops through the container and removes all children */
    if(container.childElementCount > 0){

        while(container.firstChild){

            container.removeChild(container.firstChild);
        }
    }
    red_img.src = `../images/wrong.png`;
    green_img.src = `../images/valid.png`;

    if (isValid) {
        container.append(green_img);
    } else {
        container.append(red_img);
        let p = document.createElement('p');
        let text = document.createTextNode(feedbackText)
        p.appendChild(text);
        container.appendChild(p);
    }
}




/**
 * //Alex Davila
 * 
 * This method checks that everything we entered in the form is valid 
 * and if it is it calls the setElementFeedback
 *
 * @param {HTMLElement} elem 
 * @param {String} pattern 
 * @param {String} feedback_text 
 * @returns 
 */

function validateElement(elem,pattern,feedback_text){
    let reg = new RegExp(pattern)
    let valid = reg.test(elem.value);
    let id = elem.id;
    let isElemValid = false;
   if(valid){
       isElemValid = true;
       setElementFeedback(id,'',isElemValid);
   }
   else{
       isElemValid = false;
       setElementFeedback(id,feedback_text,isElemValid);
   }
   return isElemValid;
}

/**
 * @description This function compares object key value proj_id
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareIds(a,b){
    if(a.proj_id < b.proj_id){
        return -1;
    }
    if(a.proj_id > b.proj_id){
        return 1;}
    return 0
}
/**
 * @description this Function compares object key value owner
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareOwners(a,b){
    if(a.owner < b.owner){
        return -1;
    }
    if(a.owner > b.owner){
        return 1;}
    return 0
}
/**
 * @description This function compares object key value title
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareTitles(a,b){
    if(a.title < b.title){
        return -1;
    }
    if(a.title > b.title){
        return 1;}
    return 0
}
/**
 * @description This function compares object key value category
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareCategories(a,b){
    if(a.category < b.category){
        return -1;
    }
    if(a.category > b.category){
        return 1;}
    return 0
}
/**
 * @description This function compares object key value hours sorts hours in ascending order
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
 function compareHours(array){
    array.sort((a,b) => a.hours - b.hours);
    if(arr.length<2){
        /*status bar*/
        document.getElementById("query-result").innerHTML = `Nothing to sort`;
    }
    else{
        /*Updating Status bar*/
        document.getElementById("query-result").innerHTML = `Sorted hours column, type "arr" in console to see sorted column`;
    }
}
/**
 * @description This function compares object key value rate and sorts rates in ascending order
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareRates(array){
    array.sort((a,b) => a.rate - b.rate);
    if(arr.length<2){
        /*status bar*/
        document.getElementById("query-result").innerHTML = `Nothing to sort`;
    }
    else{
    document.getElementById("query-result").innerHTML = `Sorted rates column type "arr" in console to see sorted column`;
    }
}
/**
 * @description This function compares object key value status
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareStatus(a,b){
    if(a.status < b.status){
        return -1;
    }
    if(a.status > b.status){
        return 1;}
    return 0
}
/**
 * @description This function compares object key value description
 * @param {*} a 
 * @param {*} b 
 * @returns 
 */
function compareDescriptions(a,b){
    if(a.description < b.description){
        return -1;
    }
    if(a.description > b.description){
        return 1;}
    return 0
}

/**
 * @description This function takes as input one of the compare functions to sort its columm 
 */
 function sortcolumn(array, compareFunction){
    
    array.sort(compareFunction);
    if(arr.length<2){
        /*status bar*/
        document.getElementById("query-result").innerHTML = `Nothing to sort`;
    }
    else{
    document.getElementById("query-result").innerHTML = `Sorted column type "arr" in console to see sorted column`;
    }
}

