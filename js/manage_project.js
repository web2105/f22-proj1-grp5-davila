/* 
Kayci Nicole Davila student id:2141560
Alex Alonso Davila student id:2132425
*/
'use strict';

/**
 * Alex Davila
 * 
 * This functions creates an Object project 
 * which will be used throughout the code
 * 
 * @returns the object created 
 */
function createProjectObject(){
    let project = {
        proj_id: document.querySelector("#projectId").value,
        owner: document.querySelector("#ownerName").value,
        title: document.querySelector("#title").value,
        category: document.querySelector("#category").value,
        hours: document.querySelector("#hours").value,
        rate: document.querySelector("#rate").value, 
        status: document.querySelector("#status").value,
        description: document.querySelector("#description").value,
        sequence: 0
    };
   
    return project;
 }

//Alex Davila
/**
 * This is the function that does the main actions on the table.
 * It calls the createRow, pushes to the array,saves to local storage, appends to local storage
 * and has the query eventListener inside too
 * 
 * @param {Event} event 
 * @param {Object} arr 
 */
 function updateProjectsTable(event,arr){
     event.preventDefault();
     let project_obj = createProjectObject();
     project_obj.sequence = arr.length;
     let table = document.getElementById("tb1").getElementsByTagName('tbody')[0];
     let row = table.insertRow(table.length);
    
     createRow(project_obj,row,project_obj.sequence);
     arr.push(project_obj);
     let saveToLocal = document.getElementById("save");
     saveToLocal.addEventListener('click',function(){saveToLocalStorage(arr);});

     let appendStorage = document.getElementById("append");
     appendStorage.addEventListener('click',function(){appendToLocalStorage(arr);});
     
  
     let query = document.getElementById("query");
     query.addEventListener('keyup',function(){searchTable(project_obj,query);});
     searchTable();

     

   
     
 }

 /**This function will sort the table by id*/
 function sortTable(){

 }
 
 
//Alex Davila
/**
 * This function take an object from an array and creates a table with with its values
 * and appends it to a parent element
 * 
 * @param {Object} elem 
 * @param {HTMLElement} parent 
 * @param {Number} sequence 
 */
 function createRow(elem,parent,sequence){
    parent.setAttribute('class',sequence);

    let cell1 = parent.insertCell(0);
    cell1.innerHTML = elem.proj_id;
    let cell2= parent.insertCell(1);
    cell2.innerHTML = elem.owner;
    let cell3 = parent.insertCell(2);
    cell3.innerHTML = elem.title;
    let cell4 = parent.insertCell(3);
    cell4.innerHTML = elem.category;
    let cell5 = parent.insertCell(4);
    cell5.innerHTML = elem.hours;
    let cell6 = parent.insertCell(5);
    cell6.innerHTML = elem.rate;
    let cell7 = parent.insertCell(6);
    cell7.innerHTML = elem.status;
    let cell8 = parent.insertCell(7);
    cell8.innerHTML = elem.description;
    let cell9 = parent.insertCell(8);
    cell9.innerHTML = '<img id="edit" onclick="editRows('+sequence+')" src = ../images/edit.png alt = "edit icon">';
    //cell9.onclick = editRows(this,arr);
    let cell10 = parent.insertCell(9);
    cell10.innerHTML = '<img id="del" onclick="deleteRows('+sequence+')" src = ../images/delete.png alt = "delete icon">';
    

}
//Alex Davila
/**
 * This function deletes a row from the table whenever its delete button is clicked
 * 
 * @param {Number} sequence 
 */
 function deleteRows(sequence){
    let row = document.getElementsByClassName(sequence)[0];
    /*fetching project id to display*/
    let projId = row.childNodes[0].innerText;
    if(confirm(`Are you sure you want to delete project ${projId}?`)){
     
     let table = document.getElementById("tbdy");
     table.deleteRow(sequence);
     for(let i = 0; i < table.rows.length;i++){
         table.rows[i].setAttribute('class',i);
         table.rows[i].cells[8].innerHTML = '<img id="edit" onclick="editRows('+i+')" src = ../images/edit.png alt="edit icon">';
         table.rows[i].cells[9].innerHTML = '<img id="del" onclick="deleteRows('+i+')" src = ../images/delete.png alt ="delete icon">';
     }
     arr = deleteArrayElem(sequence);
     console.log(arr);

     /*Updating Status bar*/
     document.getElementById("query-result").innerHTML = `Deleted project ${projId}`;
     
    }
 }

 //Alex Davila
 /**
  * This function will delete from the array the element deleted from the table
  * 
  * @param {Number} sequence 
  * @returns an array without the element deleted
  */
 function deleteArrayElem(sequence){
        let editedArray = [];
        let index = 0;
        arr.forEach(function(){
            if(sequence !== index){
                let elem = arr[index];
                elem.sequence = editedArray.length;
                editedArray.push(elem);
            }
            
            index++;
        });
         return editedArray;
 }
 


//Alex Davila
/**
 * This function allows to edit the info of the the selected row
 * 
 * @param {Number} sequence 
 */
 function editRows(sequence){
    let row = document.getElementsByClassName(sequence)[0].cells;
    for(let i = 0; i < 8; i++){
        let value = row[i].innerHTML;
        row[i].innerHTML="<input type='text' class='edited' value='"+value+"'>";
        row[i].value = value;
    }
    row[8].innerHTML = '<img id="sv" onclick="saveRow(this,'+sequence+')" src = ../images/save.png alt="save icon" >';

 }

 //Alex Davila
 /**
  * This function is the button that display after the dit button is clicked
  * it allows to save the new info into the array after its clicked
  * 
  * @param {HTMLElement} cell 
  * @param {Number} sequence 
  */
 function saveRow(cell,sequence){
    let row = cell.parentElement.parentElement;
    let vals = document.querySelectorAll(".edited");

    for(let i = 0; i < 8; i++){
        for(let i = 0; i < vals.length; i++){
            row.cells[i].innerHTML = vals[i].value;
        }

    }
    row.cells[8].innerHTML = '<img id="ed" onclick="editRows('+sequence+')" src = ../images/edit.png alt="edit icon">';

    let elemobj = createProjectObject();

    elemobj.proj_id = row.cells[0].innerHTML;
    elemobj.owner = row.cells[1].innerHTML;
    elemobj.title = row.cells[2].innerHTML;
    elemobj.category = row.cells[3].innerHTML;
    elemobj.hours = row.cells[4].innerHTML;
    elemobj.rate = row.cells[5].innerHTML;
    elemobj.status = row.cells[6].innerHTML;
    elemobj.description = row.cells[7].innerHTML;
    elemobj.sequence = sequence;
    
    arr = modifyArray(elemobj); 
    /*Updating status bar */
    document.getElementById("query-result").innerHTML = `Edited project ${elemobj.proj_id}`;

 }


 /**
  * Alex Davila
  * 
  * This function takes an object then for each element of the global array
  * it checks the sequence value which is a number that goes up by one for every new row 
  * 
  * If the index is equal to the sequence we push that object into the array, if its not we just
  * push the nornal value of the array
  * 
  * this allows to edit any value inside of the array
  * 
  * @param {Object} elemobj 
  * @returns 
  */
 function modifyArray(elemobj){
    let editedArray = [];
    let index = 0;

    arr.forEach(function(){
        if(elemobj.sequence === index){
            editedArray.push(elemobj);
        }
        else{
            editedArray.push(arr[index]);
        }
        index++;
    });
    return editedArray;
 }


 /**
  * Alex Davila
  * 
  * This functions simply saves the array of object in Local Storage 
  * overriding what was before
  */
 function saveToLocalStorage(){
        console.log(arr);
        localStorage.setItem('projects',JSON.stringify(arr));
 }

 /**
  * Alex Davila
  * 
  * This function is the function that reads from Local Storage when the load from local
  * button is clicked this function creates a new table with the array saved in local
  */
 function readFromLocalStorage(){
     if(localStorage.getItem('projects') != null){
        let table = document.getElementById("tbdy");
        
        let proj = JSON.parse(localStorage.getItem('projects'));
     
          //here we take every value  of the proj array and insert a cell with its content
          //creating a new row everytime
          proj.forEach(row=>{
            let newRow = table.insertRow()
                    console.log(row.sequence);
                    newRow.setAttribute('class',row.sequence);
                    let cell1 = newRow.insertCell();
                    cell1.innerHTML = row.proj_id;
                    let cell2= newRow.insertCell();
                    cell2.innerHTML = row.owner;
                    let cell3 = newRow.insertCell();
                    cell3.innerHTML = row.title;
                    let cell4 = newRow.insertCell();
                    cell4.innerHTML = row.category;
                    let cell5 = newRow.insertCell();
                    cell5.innerHTML = row.hours;
                    let cell6 = newRow.insertCell();
                    cell6.innerHTML = row.rate;
                    let cell7 = newRow.insertCell();
                    cell7.innerHTML = row.status;
                    let cell8 = newRow.insertCell();
                    cell8.innerHTML = row.description;
                    let cell9 = newRow.insertCell();
                    cell9.innerHTML = '<img id="edit" onclick="editRows('+row.sequence+')" src = ../images/edit.png alt="edit icon">';
                    //cell9.onclick = editRows(this,arr);
                    let cell10 = newRow.insertCell();
                    cell10.innerHTML = '<img id="del" onclick="deleteRows('+row.sequence+')" src = ../images/delete.png alt="delete icon">';
                    console.log(cell10);

                    let query = document.getElementById("query");
                    query.addEventListener('keyup',function(){searchTable(project_obj,query);});
                    searchTable();
        }) 

     }
 }
 /**
  * This method appends a new array to the old one
  * Its starts by putting the old value in a variable called oldVal then I create
  * a new data key for local storage called project and store the new array I want to append
  * in it.
  * I then push that new value into the old array from projects and it appends that array at the end 
  * of the projects storage which was the original 
  */
 function appendToLocalStorage(arr){
        let oldVal = JSON.parse(localStorage.getItem('projects'));
        if(oldVal === null) {
            oldVal = [];
        }
        for (let value of arr){
            oldVal.push((value));
        }

        let string = JSON.stringify(oldVal);
        localStorage.setItem('projects', string);
 }
//Alex Davila
/**
 * This function clears the local storage
 */
 function clearLocalStorage(){
     localStorage.clear();
 }

 //Alex Davila
 /**
  * This functionn allows to query the table and search for specific projects depending on what is typed
  */
 function searchTable(){
    let query = document.getElementById("query");
    query.addEventListener('keyup',function(){
        let word = this.value;
        word = word.toLowerCase();
        let table = document.getElementById("tb1");
        let rows = table.getElementsByTagName("tr");
        let results = 0;
        for(let i = 0; i<rows.length;i++){
            let proj = rows[i].getElementsByTagName("td");
            for(let j = 0; j<proj.length;j++){
                if(proj[j]){
                    let val = proj[j].innerText || proj[j].textContent;
                    val = val.toLowerCase();
                    if(val.indexOf(word) > -1){
                        results++;
                        rows[i].style.display = "";
                        break;
                    }
                    else{
                        rows[i].style.display = "none";
                    }
                }
            }
          
        }

        /*updating status bar*/
        if(results<1){
            document.getElementById("query-result").innerHTML = `There are no projects for your query..`;
        }
        else if(results==1){
            document.getElementById("query-result").innerHTML = `There is 1 project for your query..`;
        }
        else{
            document.getElementById("query-result").innerHTML = `There are ${results} projects for your query..`;
        }
    });
 }  

 //Alex Davila
 /**This function allows the reset button to reset the form and make it empty again */
 function resetProject(event){
     event.preventDefault();
     let btn = document.getElementById("add");
     document.querySelector('form').reset();
     enable_disable_button(btn,false);
 }


//Alex Davila
/**
 * This function checks if every input is valid and if it is it enables the button
 */
 function validateAll(){
  
    let proj_pattern = new RegExp(/^[a-zA-Z]{1}[\w$_\-]{2,9}$/);
    let owner_pattern = new RegExp(/^[a-zA-Z]{1}[\w\-]{2,9}$/);
    let proj_title = new RegExp(/^[a-zA-Z]{3,25}/);
    let category_patt= new RegExp(/^[a-zA-Z]{3,25}/);
    let status_patt= new RegExp(/^[a-zA-Z]{3,25}/);
    let nber_hours = new RegExp(/^[0-9]{1,3}$/);
    let rate_pattern = new RegExp(/^[0-9]{1,3}$/);
    let proj_descr =  new RegExp(/^[\s\S]{3,65}$/);

        let project = document.querySelector("#projectId");
        let owner = document.querySelector("#ownerName");
        let title = document.querySelector("#title");
        let category = document.querySelector("#category");
        let hours = document.querySelector("#hours");
        let rate = document.querySelector("#rate");
        let status = document.querySelector("#status");
        let description = document.querySelector("#description");

        let projValid = false;
        let ownerValid = false;
        let titleValid = false;
        let catValid = false;
        let hoursValid = false;
        let rateValid = false;
        let statValid = false;
        let descrValid = false;

        if (proj_pattern.test(project.value)) {
            projValid = true;
        }
        
        if (owner_pattern.test(owner.value)) {
      
            ownerValid = true;
        }
        
        if (proj_title.test(title.value)) {  

            titleValid = true;
        } 
        
        if (category_patt.test(category.value)) { 
            catValid = true;
        } 
        
        if (nber_hours.test(hours.value)){ 
             hoursValid = true;
        }   
        
        if (rate_pattern.test(rate.value)) { 
            rateValid = true;
        }
        
        if (status_patt.test(status.value)) {
            statValid = true;
        }
        
        if (proj_descr.test(description.value)) { 
            descrValid = true;
        } 

        let btn = document.getElementById("add");
        
          if(projValid && ownerValid && titleValid && catValid && hoursValid && rateValid && statValid && descrValid){
              enable_disable_button(btn,true);
          }
          else{
              enable_disable_button(btn,false);
          }
        
  }
 
 
