Project Content:
HTML:
Our project has a main html file called main-page.html. It creates our form section as well as our table header and our page footer.
CSS:
We have two css pages, one to style our main page and one to style our table.
JavaScript:
We have three JS files to make our page function properly. We have a utilities file that is like a library that consists of small functions. We also have manage_program and main_project.
FUNCTIONS IN UTILITIES: 
-enable_disable_button

-setRowColors

-set_elem_required

-setElementFeedback

-validateElement


FUNCTIONS IN MANAGE_PROJECTS:
-createProjectObject

-updateProjectsTable

-createRow

-deleteRows

-deleteArrayElem

-editRows

-saveRow

-modifyArray

-saveToLocalStorage

-readFromLocalStorage

-appendToLocalStorage

-clearLocalStorage

-searchTable

-resetProject

-validateAll

FUNCTIONS IN MAIN_PROGRAM:

- Declaring every object (for id's)
- Declaring the regex patterns
- initialize every global variable(array,buttons,etc..)
- DOMContentLoaded
- event listeners for the form inputs


HOW TO RUN OUR PRGOGRAM

1: Fill out the form on our web page, and make sure that everything is valid (if it is not it will not work);

2: If everything is valid the add button will be enabled and clickable

3: Click the add button and a table row will display with every information you've inputed in the form

4: As seen there are two buttons at the end of each row, one is the edit button which allows to edit the row directly when clicked. When the edit button is clicked another button will replace it, the save button(simply saves the changes you made to the row). There is also the delete button, which deletes the specific row clicked.

5: We also have a query bar, which is useful when you have mutliple projects and we want to display a specific project or projects, since it filters through the projects and only selects the ones you want.

6: We have some other buttons buttons besides the add one. 

Reset Button: when clicked resets the form and makes it empty again.
Write: Saves the current table of projects to local storage and overrides any existing;
Append: Appends the table to an existing already saved table in local storage
Clear: Clears the local storage deleting everything from it
Load: Reads the value of local storage and displays everything from it in a table

7: We have a sort icon that when clicked will sort the projects array, you will be able to see the contents of the sorted array when you type arr in the console (inside developper tools).









